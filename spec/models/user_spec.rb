require 'rails_helper'
 
RSpec.describe User, type: :model do
    # pending "add some examples to (or delete) #{__FILE__}"
    describe User do        

           it { should belong_to(:country) }
           
           it { should validate_presence_of(:username) }
           it { should validate_presence_of(:email) }
           it { should validate_presence_of(:password) }
           

           it { should validate_uniqueness_of(:email).ignoring_case_sensitivity.with_message('is invalid') }
           it { should validate_uniqueness_of(:username).ignoring_case_sensitivity }


           it { should validate_inclusion_of(:active_otp).in_array([1, 2])}
           it { should validate_inclusion_of(:account_currency).in_array([true, false])}
           it { should validate_inclusion_of(:roleid).in_array([1, 2])}

           #it { should validate_numericality_of(:telephone)}
         

    end
  end

  

