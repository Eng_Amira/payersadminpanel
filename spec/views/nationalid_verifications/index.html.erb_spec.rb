require 'rails_helper'

RSpec.describe "nationalid_verifications/index", type: :view do
  before(:each) do
    assign(:nationalid_verifications, [
      NationalidVerification.create!(
        :user_id => 2,
        :legal_name => "Legal Name",
        :national_id => 3,
        :type => 4,
        :status => 5,
        :note => "Note"
      ),
      NationalidVerification.create!(
        :user_id => 2,
        :legal_name => "Legal Name",
        :national_id => 3,
        :type => 4,
        :status => 5,
        :note => "Note"
      )
    ])
  end

  it "renders a list of nationalid_verifications" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Legal Name".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => "Note".to_s, :count => 2
  end
end
