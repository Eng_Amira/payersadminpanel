class AddressVerification < ApplicationRecord
    has_one_attached :attachment
    belongs_to :user ,:foreign_key => "user_id"
    belongs_to :country ,:foreign_key => "country_id"
    has_one :user_info , foreign_key: "nationalid_verification_id"
    enum status: { UnVerified: 0, Verified: 1 }


    validates :city, presence: true, length: { minimum: 3, maximum: 20 }
    validates :state, presence: true, length: { minimum: 3, maximum: 20 }
    validates :street, presence: true, length: { minimum: 3, maximum: 20 }
    validates :apartment_number, presence: true, length: { minimum: 1, maximum: 10 }
    validates_format_of :city, :with => /\A[^`'!@#\$%\^&*+_=<>]+\z/
    validates_format_of :state, :with => /\A[^`'!@#\$%\^&*+_=<>]+\z/
    validates_format_of :street, :with => /\A[^`'!@#\$%\^&*+_=<>]+\z/
    validates_format_of :apartment_number, :with => /\A[^`'!@#\$%\^&*+_=<>]+\z/
    validates_format_of :note, :with => /\A[^`!@#\$%\^&*+_=<>]+\z/, if: :validate_note?

    

    validate :attachment_type
   

    def attachment_type
        if attachment.attached? && !attachment.attachment.blob.content_type.in?(%w(image/png image/jpeg))
            errors.add(:attachment, 'Must be an image file or extension is not included in the list')
        elsif !attachment.attached?
            errors.add(:attachment, 'attachment is required')
        end
        
        #if attachment.attached? && !attachment.content_type.in?(%w(application/msword application/pdf))
         # errors.add(:attachment, 'Must be a PDF or a DOC file')
        #end
    end

    def validate_note?
        note.present?
    end

    
end
