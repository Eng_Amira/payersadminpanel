class NationalidVerification < ApplicationRecord
    has_one_attached :attachment1
    has_one_attached :attachment2
    has_many_attached :images
    belongs_to :user ,:foreign_key => "user_id"
    has_one :user_info , foreign_key: "nationalid_verification_id"

    enum document_type: { NationalId_Card: 1, Passport: 2, Driving_License: 3}
    enum status: { UnVerified: 0, Verified: 1 }

    validates :legal_name, presence: true, length: { minimum: 10, maximum: 50 }
    validates :document_type, presence: true
    validates_format_of :legal_name, :with => /\A[^0-9`!@#\$%\^&*+_=<>]+\z/    
    validates :issue_date, :timeliness => {:on_or_before => lambda { Date.current }, :type => :date}
    validates :expire_date, :timeliness => {:after => lambda { Date.current }, :type => :date}
    
    validate :attachment_type

    validates_format_of :national_id, :with => /\A[^`!@#\$%\^&*+_=<>]+\z/
    validates :national_id, presence: true, :numericality => true, :length => {:maximum => 20 }, :unless => :should_validate_document_type?
    validates :national_id, presence: true, :length => {:minimum => 8 }, if: :should_validate_document_type?
    validates :attachment1, presence: true
    validates :attachment2, presence: true, :unless => :should_validate_document_type?

    validates_format_of :note, :with => /\A[^`!@#\$%\^&*+_=<>]+\z/, if: :validate_note?
    
    private

        def attachment_type
            if attachment1.attached? && !attachment1.attachment.blob.content_type.in?(%w(image/png image/jpeg))
                errors.add(:attachment1, 'Must be an image file or extension is not included in the list')
            end
            if attachment2.attached? && !attachment2.attachment.blob.content_type.in?(%w(image/png image/jpeg))
                errors.add(:attachment2, 'Must be an image file or extension is not included in the list')
            end
            #if attachment1.attached? && !attachment1.content_type.in?(%w(application/msword application/pdf))
             # errors.add(:attachment1, 'Must be a PDF or a DOC file')
            #end
        end

        def should_validate_document_type?
            if document_type == "Passport"
                return true
            else
                return false
            end 
        end

        def validate_note?
            note.present?
        end

        
end
