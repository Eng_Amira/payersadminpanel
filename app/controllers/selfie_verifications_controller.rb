class SelfieVerificationsController < ApplicationController
  before_action :set_selfie_verification, only: [:show, :edit, :update, :destroy]
  before_action :require_login


  # show list of all Selfie Verifications,
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [String] note
  def index
    if current_user.roleid == 1 
        @selfie_verifications = SelfieVerification.all
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # show details of Selfie Verification of specific user
  # @param [Integer] id 
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at 
  def show
    if current_user.roleid != 1 and current_user.id != @selfie_verification.user_id.to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new Selfie Verification
  # each user can create only one Selfie verification
  def new
    @current_user_verification = SelfieVerification.where("user_id =?", current_user.id.to_i).first
    if @current_user_verification != nil
      redirect_to root_path , notice: "not allowed"
    else
    @selfie_verification = SelfieVerification.new
    end
  end

  # edit Selfie Verification
  # user can edit his Selfie verification only if status is not verified
  # @param [Integer] id
  def edit
    if (current_user.roleid != 1 and @selfie_verification.status == "Verified") or (current_user.roleid != 1 and current_user.id != @selfie_verification.user_id.to_i)
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new Selfie Verification
  # @param [Blob] avatar
  # @return [Integer] id
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    @selfie_verification = SelfieVerification.new(selfie_verification_params)
    @selfie_verification.user_id = current_user.id
    respond_to do |format|
      if @selfie_verification.save
        format.html { redirect_to @selfie_verification, notice: 'Selfie verification was successfully created.' }
        format.json { render :show, status: :created, location: @selfie_verification }
      else
        format.html { render :new }
        format.json { render json: @selfie_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  # edit Selfie Verification,
  # only admins can edit status and note.
  # @param [Blob] avatar
  # @param [Integer] status
  # @param [String] note
  # @return [Integer] id
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    respond_to do |format|
      if @selfie_verification.update(selfie_verification_params)
        if current_user.roleid == 1
          @userinfo = UserInfo.where("user_id =?", @selfie_verification.user_id ).first
          if params[:selfie_verification][:status] == "Verified"
             if @userinfo == nil              
                 UserInfo.create(:user_id => @selfie_verification.user_id , :nationalid_verification_id => 0, :address_verification_id => 0, :selfie_verification_id => @selfie_verification.id, :status => "UnVerified")
             else
              @userinfo.update(:selfie_verification_id => @selfie_verification.id)
              @user_nationalid_verification = NationalidVerification.where("user_id =?", @selfie_verification.user_id ).first
              if @user_nationalid_verification != nil and @user_nationalid_verification.status == "Verified"
                 @user_address_verification = AddressVerification.where("user_id =?", @selfie_verification.user_id ).first
                 if @user_address_verification != nil and @user_address_verification.status == "Verified"
                  @userinfo.update(:status => "Verified")
                 end
              end
             end
          elsif params[:selfie_verification][:status] == "UnVerified" and @userinfo != nil
              @userinfo.update(:status => "UnVerified")
          end
        end
        format.html { redirect_to @selfie_verification, notice: 'Selfie verification was successfully updated.' }
        format.json { render :show, status: :ok, location: @selfie_verification }
      else
        format.html { render :edit }
        format.json { render json: @selfie_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /selfie_verifications/1
  # DELETE /selfie_verifications/1.json
  def destroy
    @selfie_verification.destroy
    respond_to do |format|
      format.html { redirect_to selfie_verifications_url, notice: 'Selfie verification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_selfie_verification
      @selfie_verification = SelfieVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def selfie_verification_params
      params.require(:selfie_verification).permit(:user_id, :note, :avatar, :status)
    end
end
