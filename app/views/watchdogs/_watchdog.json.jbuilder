json.extract! watchdog, :id, :admin_id, :logintime, :ipaddress, :lastvisit, :created_at, :updated_at
json.url watchdog_url(watchdog, format: :json)
