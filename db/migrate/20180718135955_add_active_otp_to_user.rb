class AddActiveOtpToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :active_otp, :integer, :default => 1

  end
end
