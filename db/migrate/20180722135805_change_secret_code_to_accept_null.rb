class ChangeSecretCodeToAcceptNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null :users, :secret_code, true
  end
end
