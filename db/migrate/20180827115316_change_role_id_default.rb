class ChangeRoleIdDefault < ActiveRecord::Migration[5.2]
  def change
    change_column_default :users, :roleid, 2
  end
end
